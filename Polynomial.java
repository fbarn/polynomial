package a2;

import java.math.BigInteger;

public class Polynomial
{
	private SLinkedList<Term> polynomial;
	public int size()
	{
		return polynomial.size();
	}
	private Polynomial(SLinkedList<Term> p)
	{
		polynomial = p;
	}

	public Polynomial()
	{
		polynomial = new SLinkedList<Term>();
	}

	// Returns a deep copy of the object.
	public Polynomial deepClone()
	{
		return new Polynomial(polynomial.deepClone());
	}

	public void addTerm(Term t)
	{
		int i=0;
		for (Term currentTerm: polynomial)
		{
			if (t.getExponent()==currentTerm.getExponent()){
				currentTerm.setCoefficient(currentTerm.getCoefficient().add(t.getCoefficient()));
				if (currentTerm.getCoefficient().equals(new BigInteger("0"))){
					polynomial.remove(i);
				}
				return;
			}
			if (t.getExponent() > currentTerm.getExponent()){
				polynomial.add(i, t);
				return;
			}
			i+=1;
		}
		polynomial.add(i,t);
	}

	public Term getTerm(int index)
	{
		return polynomial.get(index);
	}

	public static Polynomial add(Polynomial p1, Polynomial p2)
	{
		Polynomial p3=p1.deepClone();
		for (Term t : p2.polynomial){
			p3.addTerm(t);
		}
		return p3;
	}

	private void multiplyTerm(Term t)
	{
		if (t.getCoefficient().equals(new BigInteger("0"))){
			polynomial.clear();
		}
		for (Term u : polynomial){
			u.setCoefficient(u.getCoefficient().multiply(t.getCoefficient()));
			u.setExponent(u.getExponent()+t.getExponent());
		}
	}

	public static Polynomial multiply(Polynomial p1, Polynomial p2)
	{
		Polynomial p3= new Polynomial();
		for (Term t : p2.polynomial){
			Polynomial p4=p1.deepClone();
			p4.multiplyTerm(t);
			p3=Polynomial.add(p3,p4);
		}
		return p3;
	}

	public BigInteger eval(BigInteger x)
	{
		BigInteger b=new BigInteger("0");
		int prev=0;
		int i=0;
		int k=0;
		for (Term t : polynomial){
			if (i>0){
				int diff=prev-t.getExponent()-1;
				for (int j=0; j<diff; j++){
					b=b.multiply(x);
				}
			}
			else{
				i++;
			}
			b=t.getCoefficient().add(b.multiply(x));
			prev=t.getExponent();
			k=t.getExponent();
		}
		for (int j=k; j>0; j--){
			b=b.multiply(x);
		}
		return b;
	}

	// Checks if this polynomial is same as the polynomial in the argument
	public boolean checkEqual(Polynomial p)
	{
		if (polynomial == null || p.polynomial == null || size() != p.size())
			return false;

		int index = 0;
		for (Term term0 : polynomial)
		{
			Term term1 = p.getTerm(index);

			if (term0.getExponent() != term1.getExponent() ||
				term0.getCoefficient().compareTo(term1.getCoefficient()) != 0 || term1 == term0)
					return false;

			index++;
		}
		return true;
	}

	// This method blindly adds a term to the end of LinkedList polynomial.
	// Avoid using this method in your implementation as it is only used for testing.
	public void addTermLast(Term t)
	{
		polynomial.addLast(t);
	}

	// This is used for testing multiplyTerm
	public void multiplyTermTest(Term t)
	{
		multiplyTerm(t);
	}

	@Override
	public String toString()
	{
		if (polynomial.size() == 0) return "0";
		return polynomial.toString();
	}
}
